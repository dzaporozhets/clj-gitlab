(ns clj-gitlab.files
  (:use [clj-gitlab.core :only [api-get]]))

(defn- repository-path [project-id]
  (str "/projects/" project-id "/repository"))

(defn tree [project-id & [options]]
  (api-get (str (repository-path project-id) "/tree") options))
