# Change Log
All notable changes to this project will be documented in this file. This change log follows the conventions of [keepachangelog.com](http://keepachangelog.com/).

## [0.2.0]
### Added
- `/diff` and `/comments` endpoints to Commits API
- Files API
### Changed
- Updated all dependencies to receont one

## [0.1.0]
### Added
- Projects API
- Issues API
- Commits API
- Private/personal token support
