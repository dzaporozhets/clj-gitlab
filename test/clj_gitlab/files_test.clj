(ns clj-gitlab.files-test
  (:require [clojure.test :refer :all]
            [clj-gitlab.files :refer :all]
            [vcr-clj.clj-http :refer [with-cassette]]))

(deftest tree-test
  (testing "GET /projects/:id/repository/tree endpoint for anonymous user"
    (with-cassette :files
      (is (seq (tree "13083"))))))
