(ns clj-gitlab.commits-test
  (:require [clojure.test :refer :all]
            [clj-gitlab.commits :refer :all]
            [vcr-clj.clj-http :refer [with-cassette]]))

(deftest commits-test
  (testing "GET /projects/:id/repository/commits endpoint for anonymous user"
    (with-cassette :commits
      (is (seq (commits "13083"))))))

(deftest commit-test
  (testing "GET /projects/:id/repository/commits/:sha endpoint for anonymous user"
    (with-cassette :commit
      (is (=
           (get (commit "13083" "a019369409e38ee6cb36e5782cefd8c944b12e53") "short_id")
           "a0193694")))))

(deftest commit-diff-test
  (testing "GET /projects/:id/repository/commits/:sha/diff endpoint for anonymous user"
    (with-cassette :commit-diff
      (is (=
           (get (first (commit-diff "13083" "a019369409e38ee6cb36e5782cefd8c944b12e53")) "old_path")
           "changelogs/unreleased/dm-simple-project-avatar-url.yml")))))

(deftest commit-comments-test
  (testing "GET /projects/:id/repository/commits/:sha/comments endpoint for anonymous user"
    (with-cassette :commit-comments
      (is (=
           (get (first (commit-comments "4186769" "c857a78d29f1e144921fad2a8799a97f4f6a7cff")) "note")
           "Nice one")))))
