{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/4202845/issues/1"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NjkwNjA1NCwiaWlkIjoxLCJwcm9qZWN0X2lkIjo0MjAyODQ1LCJ0aXRsZSI6Ik9wZW4"
                    "gaXNzdWUiLCJkZXNjcmlwdGlvbiI6IiIsInN0YXRlIjoib3BlbmVkIiwiY3JlYXRlZF9hdCI6Ij"
                    "IwMTctMDktMjFUMTM6MTg6MzQuODQyWiIsInVwZGF0ZWRfYXQiOiIyMDE3LTA5LTIxVDEzOjE4O"
                    "jM0Ljg0MloiLCJsYWJlbHMiOltdLCJtaWxlc3RvbmUiOm51bGwsImFzc2lnbmVlcyI6W10sImF1"
                    "dGhvciI6eyJpZCI6Mzg1MjcsIm5hbWUiOiJEb2dlIERldiIsInVzZXJuYW1lIjoiZG9nZWRldiI"
                    "sInN0YXRlIjoiYWN0aXZlIiwiYXZhdGFyX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmNvbS91cGxvYW"
                    "RzLy0vc3lzdGVtL3VzZXIvYXZhdGFyLzM4NTI3L2RvZ2UuanBlZyIsIndlYl91cmwiOiJodHRwc"
                    "zovL2dpdGxhYi5jb20vZG9nZWRldiJ9LCJhc3NpZ25lZSI6bnVsbCwidXNlcl9ub3Rlc19jb3Vu"
                    "dCI6MCwidXB2b3RlcyI6MCwiZG93bnZvdGVzIjowLCJkdWVfZGF0ZSI6bnVsbCwiY29uZmlkZW5"
                    "0aWFsIjpmYWxzZSwid2VpZ2h0IjpudWxsLCJ3ZWJfdXJsIjoiaHR0cHM6Ly9naXRsYWIuY29tL2"
                    "RvZ2VkZXYvdGVzdC1jbGktZ2l0bGFiL2lzc3Vlcy8xIiwidGltZV9zdGF0cyI6eyJ0aW1lX2Vzd"
                    "GltYXRlIjowLCJ0b3RhbF90aW1lX3NwZW50IjowLCJodW1hbl90aW1lX2VzdGltYXRlIjpudWxs"
                    "LCJodW1hbl90b3RhbF90aW1lX3NwZW50IjpudWxsfSwiX2xpbmtzIjp7InNlbGYiOiJodHRwOi8"
                    "vZ2l0bGFiLmNvbS9hcGkvdjQvcHJvamVjdHMvNDIwMjg0NS9pc3N1ZXMvMSIsIm5vdGVzIjoiaH"
                    "R0cDovL2dpdGxhYi5jb20vYXBpL3Y0L3Byb2plY3RzLzQyMDI4NDUvaXNzdWVzLzEvbm90ZXMiL"
                    "CJhd2FyZF9lbW9qaSI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjAyODQ1"
                    "L2lzc3Vlcy8xL2F3YXJkX2Vtb2ppIiwicHJvamVjdCI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS9"
                    "2NC9wcm9qZWN0cy80MjAyODQ1In0sInN1YnNjcmliZWQiOnRydWV9"],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "996",
                    "Content-Type" "application/json",
                    "Date" "Fri, 22 Sep 2017 13:40:31 GMT",
                    "Etag" "W/\"48fe4c7dcef92bfc1a8e826c2b745f01\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "2",
                    "RateLimit-Remaining" "598",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "e12c79e9-8209-4628-9a15-42e903730591",
                    "X-Runtime" "0.233108"},
                   :length 996,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-22T13:40:26.544-00:00"}
