{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/13083/repository/commits/a019369409e38ee6cb36e5782cefd8c944b12e53"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6ImEwMTkzNjk0MDllMzhlZTZjYjM2ZTU3ODJjZWZkOGM5NDRiMTJlNTMiLCJzaG9ydF9"
                    "pZCI6ImEwMTkzNjk0IiwidGl0bGUiOiJFeHBvc2UgYXZhdGFyX3VybCB3aGVuIHJlcXVlc3Rpbm"
                    "cgbGlzdCBvZiBwcm9qZWN0cyBmcm9tIEFQSSB3aXRoIHNpbXBsZT10cnVlIiwiY3JlYXRlZF9hd"
                    "CI6IjIwMTctMDktMjZUMTI6NDc6NDguMDAwKzAyOjAwIiwicGFyZW50X2lkcyI6WyI5MjNhZDlh"
                    "OWUyZmZjYTU1MGEwNmQwY2JiZDg5YWM2ZjI3NmM0NjA1Il0sIm1lc3NhZ2UiOiJFeHBvc2UgYXZ"
                    "hdGFyX3VybCB3aGVuIHJlcXVlc3RpbmcgbGlzdCBvZiBwcm9qZWN0cyBmcm9tIEFQSSB3aXRoIH"
                    "NpbXBsZT10cnVlXG4iLCJhdXRob3JfbmFtZSI6IkRvdXdlIE1hYW4iLCJhdXRob3JfZW1haWwiO"
                    "iJkb3V3ZUBzZWxlbmlnaHQubmwiLCJhdXRob3JlZF9kYXRlIjoiMjAxNy0wOS0yNlQxMDo0OTo1"
                    "My4wMDArMDI6MDAiLCJjb21taXR0ZXJfbmFtZSI6IkRvdXdlIE1hYW4iLCJjb21taXR0ZXJfZW1"
                    "haWwiOiJkb3V3ZUBzZWxlbmlnaHQubmwiLCJjb21taXR0ZWRfZGF0ZSI6IjIwMTctMDktMjZUMT"
                    "I6NDc6NDguMDAwKzAyOjAwIiwic3RhdHMiOnsiYWRkaXRpb25zIjoxNCwiZGVsZXRpb25zIjo1L"
                    "CJ0b3RhbCI6MTl9LCJzdGF0dXMiOiJzdWNjZXNzIn0="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "650",
                    "Content-Type" "application/json",
                    "Date" "Tue, 26 Sep 2017 15:47:45 GMT",
                    "Etag" "W/\"904a22301ff949157939d01536d8327a\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "6",
                    "RateLimit-Remaining" "594",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "6f0465e1-1df0-4ef4-941b-015a886670b2",
                    "X-Runtime" "0.689672"},
                   :length 650,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-26T15:47:42.480-00:00"}
