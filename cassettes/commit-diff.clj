{:calls [{:arg-key {:query-string nil,
                    :request-method :get,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects/13083/repository/commits/a019369409e38ee6cb36e5782cefd8c944b12e53/diff"},
          :return {:body #vcr-clj/input-stream
                   ["W3sib2xkX3BhdGgiOiJjaGFuZ2Vsb2dzL3VucmVsZWFzZWQvZG0tc2ltcGxlLXByb2plY3QtYXZ"
                    "hdGFyLXVybC55bWwiLCJuZXdfcGF0aCI6ImNoYW5nZWxvZ3MvdW5yZWxlYXNlZC9kbS1zaW1wbG"
                    "UtcHJvamVjdC1hdmF0YXItdXJsLnltbCIsImFfbW9kZSI6IjAiLCJiX21vZGUiOiIxMDA2NDQiL"
                    "CJuZXdfZmlsZSI6dHJ1ZSwicmVuYW1lZF9maWxlIjpmYWxzZSwiZGVsZXRlZF9maWxlIjpmYWxz"
                    "ZSwiZGlmZiI6Ii0tLSAvZGV2L251bGxcbisrKyBiL2NoYW5nZWxvZ3MvdW5yZWxlYXNlZC9kbS1"
                    "zaW1wbGUtcHJvamVjdC1hdmF0YXItdXJsLnltbFxuQEAgLTAsMCArMSw1IEBAXG4rLS0tXG4rdG"
                    "l0bGU6IEV4cG9zZSBhdmF0YXJfdXJsIHdoZW4gcmVxdWVzdGluZyBsaXN0IG9mIHByb2plY3RzI"
                    "GZyb20gQVBJIHdpdGggc2ltcGxlPXRydWVcbittZXJnZV9yZXF1ZXN0OlxuK2F1dGhvcjpcbit0"
                    "eXBlOiBhZGRlZFxuIn0seyJvbGRfcGF0aCI6ImxpYi9hcGkvZW50aXRpZXMucmIiLCJuZXdfcGF"
                    "0aCI6ImxpYi9hcGkvZW50aXRpZXMucmIiLCJhX21vZGUiOiIxMDA2NDQiLCJiX21vZGUiOiIxMD"
                    "A2NDQiLCJuZXdfZmlsZSI6ZmFsc2UsInJlbmFtZWRfZmlsZSI6ZmFsc2UsImRlbGV0ZWRfZmlsZ"
                    "SI6ZmFsc2UsImRpZmYiOiItLS0gYS9saWIvYXBpL2VudGl0aWVzLnJiXG4rKysgYi9saWIvYXBp"
                    "L2VudGl0aWVzLnJiXG5AQCAtODksNiArODksOSBAQCBtb2R1bGUgQVBJXG4gICAgICAgZXhwb3N"
                    "lIDpzc2hfdXJsX3RvX3JlcG8sIDpodHRwX3VybF90b19yZXBvLCA6d2ViX3VybFxuICAgICAgIG"
                    "V4cG9zZSA6bmFtZSwgOm5hbWVfd2l0aF9uYW1lc3BhY2VcbiAgICAgICBleHBvc2UgOnBhdGgsI"
                    "DpwYXRoX3dpdGhfbmFtZXNwYWNlXG4rICAgICAgZXhwb3NlIDphdmF0YXJfdXJsIGRvIHxwcm9q"
                    "ZWN0LCBvcHRpb25zfFxuKyAgICAgICAgcHJvamVjdC5hdmF0YXJfdXJsKG9ubHlfcGF0aDogZmF"
                    "sc2UpXG4rICAgICAgZW5kXG4gICAgICAgZXhwb3NlIDpzdGFyX2NvdW50LCA6Zm9ya3NfY291bn"
                    "RcbiAgICAgICBleHBvc2UgOmNyZWF0ZWRfYXQsIDpsYXN0X2FjdGl2aXR5X2F0XG4gICAgIGVuZ"
                    "FxuQEAgLTE0Niw5ICsxNDksNyBAQCBtb2R1bGUgQVBJXG4gICAgICAgZXhwb3NlIDpmb3JrZWRf"
                    "ZnJvbV9wcm9qZWN0LCB1c2luZzogRW50aXRpZXM6OkJhc2ljUHJvamVjdERldGFpbHMsIGlmOiB"
                    "sYW1iZGEgeyB8cHJvamVjdCwgb3B0aW9uc3wgcHJvamVjdC5mb3JrZWQ/IH1cbiAgICAgICBleH"
                    "Bvc2UgOmltcG9ydF9zdGF0dXNcbiAgICAgICBleHBvc2UgOmltcG9ydF9lcnJvciwgaWY6IGxhb"
                    "WJkYSB7IHxfcHJvamVjdCwgb3B0aW9uc3wgb3B0aW9uc1s6dXNlcl9jYW5fYWRtaW5fcHJvamVj"
                    "dF0gfVxuLSAgICAgIGV4cG9zZSA6YXZhdGFyX3VybCBkbyB8dXNlciwgb3B0aW9uc3xcbi0gICA"
                    "gICAgIHVzZXIuYXZhdGFyX3VybChvbmx5X3BhdGg6IGZhbHNlKVxuLSAgICAgIGVuZFxuK1xuIC"
                    "AgICAgIGV4cG9zZSA6b3Blbl9pc3N1ZXNfY291bnQsIGlmOiBsYW1iZGEgeyB8cHJvamVjdCwgb"
                    "3B0aW9uc3wgcHJvamVjdC5mZWF0dXJlX2F2YWlsYWJsZT8oOmlzc3Vlcywgb3B0aW9uc1s6Y3Vy"
                    "cmVudF91c2VyXSkgfVxuICAgICAgIGV4cG9zZSA6cnVubmVyc190b2tlbiwgaWY6IGxhbWJkYSB"
                    "7IHxfcHJvamVjdCwgb3B0aW9uc3wgb3B0aW9uc1s6dXNlcl9jYW5fYWRtaW5fcHJvamVjdF0gfV"
                    "xuICAgICAgIGV4cG9zZSA6cHVibGljX2J1aWxkcywgYXM6IDpwdWJsaWNfam9ic1xuQEAgLTE5M"
                    "yw4ICsxOTQsOCBAQCBtb2R1bGUgQVBJXG4gICAgIGNsYXNzIEdyb3VwIFx1MDAzYyBHcmFwZTo6"
                    "RW50aXR5XG4gICAgICAgZXhwb3NlIDppZCwgOm5hbWUsIDpwYXRoLCA6ZGVzY3JpcHRpb24sIDp"
                    "2aXNpYmlsaXR5XG4gICAgICAgZXhwb3NlIDpsZnNfZW5hYmxlZD8sIGFzOiA6bGZzX2VuYWJsZW"
                    "Rcbi0gICAgICBleHBvc2UgOmF2YXRhcl91cmwgZG8gfHVzZXIsIG9wdGlvbnN8XG4tICAgICAgI"
                    "CB1c2VyLmF2YXRhcl91cmwob25seV9wYXRoOiBmYWxzZSlcbisgICAgICBleHBvc2UgOmF2YXRh"
                    "cl91cmwgZG8gfGdyb3VwLCBvcHRpb25zfFxuKyAgICAgICAgZ3JvdXAuYXZhdGFyX3VybChvbmx"
                    "5X3BhdGg6IGZhbHNlKVxuICAgICAgIGVuZFxuICAgICAgIGV4cG9zZSA6d2ViX3VybFxuICAgIC"
                    "AgIGV4cG9zZSA6cmVxdWVzdF9hY2Nlc3NfZW5hYmxlZFxuIn0seyJvbGRfcGF0aCI6InNwZWMvc"
                    "mVxdWVzdHMvYXBpL2Vudmlyb25tZW50c19zcGVjLnJiIiwibmV3X3BhdGgiOiJzcGVjL3JlcXVl"
                    "c3RzL2FwaS9lbnZpcm9ubWVudHNfc3BlYy5yYiIsImFfbW9kZSI6IjEwMDY0NCIsImJfbW9kZSI"
                    "6IjEwMDY0NCIsIm5ld19maWxlIjpmYWxzZSwicmVuYW1lZF9maWxlIjpmYWxzZSwiZGVsZXRlZF"
                    "9maWxlIjpmYWxzZSwiZGlmZiI6Ii0tLSBhL3NwZWMvcmVxdWVzdHMvYXBpL2Vudmlyb25tZW50c"
                    "19zcGVjLnJiXG4rKysgYi9zcGVjL3JlcXVlc3RzL2FwaS9lbnZpcm9ubWVudHNfc3BlYy5yYlxu"
                    "QEAgLTIwLDYgKzIwLDcgQEAgZGVzY3JpYmUgQVBJOjpFbnZpcm9ubWVudHMgZG9cbiAgICAgICA"
                    "gICAgcGF0aCBwYXRoX3dpdGhfbmFtZXNwYWNlXG4gICAgICAgICAgIHN0YXJfY291bnQgZm9ya3"
                    "NfY291bnRcbiAgICAgICAgICAgY3JlYXRlZF9hdCBsYXN0X2FjdGl2aXR5X2F0XG4rICAgICAgI"
                    "CAgIGF2YXRhcl91cmxcbiAgICAgICAgIClcbiBcbiAgICAgICAgIGdldCBhcGkoXCIvcHJvamVj"
                    "dHMvI3twcm9qZWN0LmlkfS9lbnZpcm9ubWVudHNcIiwgdXNlcilcbiJ9LHsib2xkX3BhdGgiOiJ"
                    "zcGVjL3JlcXVlc3RzL2FwaS9wcm9qZWN0c19zcGVjLnJiIiwibmV3X3BhdGgiOiJzcGVjL3JlcX"
                    "Vlc3RzL2FwaS9wcm9qZWN0c19zcGVjLnJiIiwiYV9tb2RlIjoiMTAwNjQ0IiwiYl9tb2RlIjoiM"
                    "TAwNjQ0IiwibmV3X2ZpbGUiOmZhbHNlLCJyZW5hbWVkX2ZpbGUiOmZhbHNlLCJkZWxldGVkX2Zp"
                    "bGUiOmZhbHNlLCJkaWZmIjoiLS0tIGEvc3BlYy9yZXF1ZXN0cy9hcGkvcHJvamVjdHNfc3BlYy5"
                    "yYlxuKysrIGIvc3BlYy9yZXF1ZXN0cy9hcGkvcHJvamVjdHNfc3BlYy5yYlxuQEAgLTE5Myw2IC"
                    "sxOTMsNyBAQCBkZXNjcmliZSBBUEk6OlByb2plY3RzIGRvXG4gICAgICAgICAgICAgcGF0aCBwY"
                    "XRoX3dpdGhfbmFtZXNwYWNlXG4gICAgICAgICAgICAgc3Rhcl9jb3VudCBmb3Jrc19jb3VudFxu"
                    "ICAgICAgICAgICAgIGNyZWF0ZWRfYXQgbGFzdF9hY3Rpdml0eV9hdFxuKyAgICAgICAgICAgIGF"
                    "2YXRhcl91cmxcbiAgICAgICAgICAgKVxuIFxuICAgICAgICAgICBnZXQgYXBpKCcvcHJvamVjdH"
                    "M/c2ltcGxlPXRydWUnLCB1c2VyKVxuIn0seyJvbGRfcGF0aCI6InNwZWMvcmVxdWVzdHMvYXBpL"
                    "3YzL3Byb2plY3RzX3NwZWMucmIiLCJuZXdfcGF0aCI6InNwZWMvcmVxdWVzdHMvYXBpL3YzL3By"
                    "b2plY3RzX3NwZWMucmIiLCJhX21vZGUiOiIxMDA2NDQiLCJiX21vZGUiOiIxMDA2NDQiLCJuZXd"
                    "fZmlsZSI6ZmFsc2UsInJlbmFtZWRfZmlsZSI6ZmFsc2UsImRlbGV0ZWRfZmlsZSI6ZmFsc2UsIm"
                    "RpZmYiOiItLS0gYS9zcGVjL3JlcXVlc3RzL2FwaS92My9wcm9qZWN0c19zcGVjLnJiXG4rKysgY"
                    "i9zcGVjL3JlcXVlc3RzL2FwaS92My9wcm9qZWN0c19zcGVjLnJiXG5AQCAtODksNiArODksNyBA"
                    "QCBkZXNjcmliZSBBUEk6OlYzOjpQcm9qZWN0cyBkb1xuICAgICAgICAgICAgIHBhdGggcGF0aF9"
                    "3aXRoX25hbWVzcGFjZVxuICAgICAgICAgICAgIHN0YXJfY291bnQgZm9ya3NfY291bnRcbiAgIC"
                    "AgICAgICAgICBjcmVhdGVkX2F0IGxhc3RfYWN0aXZpdHlfYXRcbisgICAgICAgICAgICBhdmF0Y"
                    "XJfdXJsXG4gICAgICAgICAgIClcbiBcbiAgICAgICAgICAgZ2V0IHYzX2FwaSgnL3Byb2plY3Rz"
                    "P3NpbXBsZT10cnVlJywgdXNlcilcbiJ9XQ=="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "3850",
                    "Content-Type" "application/json",
                    "Date" "Wed, 27 Sep 2017 12:53:29 GMT",
                    "Etag" "W/\"1217d18e094c4c5b2a14fc898fe54d5e\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "4",
                    "RateLimit-Remaining" "596",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "7f8f571c-54f1-468d-9e3e-8e52e25e38b2",
                    "X-Runtime" "0.621427"},
                   :length 3850,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "OK",
                   :repeatable? false,
                   :status 200,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-27T12:53:12.427-00:00"}
