{:calls [{:arg-key {:query-string nil,
                    :request-method :post,
                    :server-name "gitlab.com",
                    :server-port nil,
                    :uri "/api/v4/projects"},
          :return {:body #vcr-clj/input-stream
                   ["eyJpZCI6NDIxMTgwNywiZGVzY3JpcHRpb24iOm51bGwsImRlZmF1bHRfYnJhbmNoIjpudWxsLCJ"
                    "0YWdfbGlzdCI6W10sInNzaF91cmxfdG9fcmVwbyI6ImdpdEBnaXRsYWIuY29tOmRvZ2VkZXYvbX"
                    "VjaC1wcm9qZWN0LmdpdCIsImh0dHBfdXJsX3RvX3JlcG8iOiJodHRwczovL2dpdGxhYi5jb20vZ"
                    "G9nZWRldi9tdWNoLXByb2plY3QuZ2l0Iiwid2ViX3VybCI6Imh0dHBzOi8vZ2l0bGFiLmNvbS9k"
                    "b2dlZGV2L211Y2gtcHJvamVjdCIsIm5hbWUiOiJtdWNoLXByb2plY3QiLCJuYW1lX3dpdGhfbmF"
                    "tZXNwYWNlIjoiRG9nZSBEZXYgLyBtdWNoLXByb2plY3QiLCJwYXRoIjoibXVjaC1wcm9qZWN0Ii"
                    "wicGF0aF93aXRoX25hbWVzcGFjZSI6ImRvZ2VkZXYvbXVjaC1wcm9qZWN0Iiwic3Rhcl9jb3Vud"
                    "CI6MCwiZm9ya3NfY291bnQiOjAsImNyZWF0ZWRfYXQiOiIyMDE3LTA5LTIyVDE2OjE2OjE3LjU0"
                    "MFoiLCJsYXN0X2FjdGl2aXR5X2F0IjoiMjAxNy0wOS0yMlQxNjoxNjoxNy41NDBaIiwiX2xpbmt"
                    "zIjp7InNlbGYiOiJodHRwOi8vZ2l0bGFiLmNvbS9hcGkvdjQvcHJvamVjdHMvNDIxMTgwNyIsIm"
                    "lzc3VlcyI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjExODA3L2lzc3Vlc"
                    "yIsIm1lcmdlX3JlcXVlc3RzIjoiaHR0cDovL2dpdGxhYi5jb20vYXBpL3Y0L3Byb2plY3RzLzQy"
                    "MTE4MDcvbWVyZ2VfcmVxdWVzdHMiLCJyZXBvX2JyYW5jaGVzIjoiaHR0cDovL2dpdGxhYi5jb20"
                    "vYXBpL3Y0L3Byb2plY3RzLzQyMTE4MDcvcmVwb3NpdG9yeS9icmFuY2hlcyIsImxhYmVscyI6Im"
                    "h0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjExODA3L2xhYmVscyIsImV2ZW50c"
                    "yI6Imh0dHA6Ly9naXRsYWIuY29tL2FwaS92NC9wcm9qZWN0cy80MjExODA3L2V2ZW50cyIsIm1l"
                    "bWJlcnMiOiJodHRwOi8vZ2l0bGFiLmNvbS9hcGkvdjQvcHJvamVjdHMvNDIxMTgwNy9tZW1iZXJ"
                    "zIn0sImFyY2hpdmVkIjpmYWxzZSwidmlzaWJpbGl0eSI6InByaXZhdGUiLCJvd25lciI6eyJpZC"
                    "I6Mzg1MjcsIm5hbWUiOiJEb2dlIERldiIsInVzZXJuYW1lIjoiZG9nZWRldiIsInN0YXRlIjoiY"
                    "WN0aXZlIiwiYXZhdGFyX3VybCI6Imh0dHBzOi8vZ2wtc3RhdGljLmdsb2JhbC5zc2wuZmFzdGx5"
                    "Lm5ldC91cGxvYWRzLy0vc3lzdGVtL3VzZXIvYXZhdGFyLzM4NTI3L2RvZ2UuanBlZyIsIndlYl9"
                    "1cmwiOiJodHRwczovL2dpdGxhYi5jb20vZG9nZWRldiJ9LCJyZXNvbHZlX291dGRhdGVkX2RpZm"
                    "ZfZGlzY3Vzc2lvbnMiOmZhbHNlLCJjb250YWluZXJfcmVnaXN0cnlfZW5hYmxlZCI6dHJ1ZSwia"
                    "XNzdWVzX2VuYWJsZWQiOnRydWUsIm1lcmdlX3JlcXVlc3RzX2VuYWJsZWQiOnRydWUsIndpa2lf"
                    "ZW5hYmxlZCI6dHJ1ZSwiam9ic19lbmFibGVkIjp0cnVlLCJzbmlwcGV0c19lbmFibGVkIjp0cnV"
                    "lLCJzaGFyZWRfcnVubmVyc19lbmFibGVkIjp0cnVlLCJsZnNfZW5hYmxlZCI6dHJ1ZSwiY3JlYX"
                    "Rvcl9pZCI6Mzg1MjcsIm5hbWVzcGFjZSI6eyJpZCI6NDM4MjksIm5hbWUiOiJkb2dlZGV2Iiwic"
                    "GF0aCI6ImRvZ2VkZXYiLCJraW5kIjoidXNlciIsImZ1bGxfcGF0aCI6ImRvZ2VkZXYiLCJwYXJl"
                    "bnRfaWQiOm51bGx9LCJpbXBvcnRfc3RhdHVzIjoibm9uZSIsImltcG9ydF9lcnJvciI6bnVsbCw"
                    "iYXZhdGFyX3VybCI6bnVsbCwib3Blbl9pc3N1ZXNfY291bnQiOjAsInJ1bm5lcnNfdG9rZW4iOi"
                    "J3YXpOd3FZa0FXZHFRdVd1ZkhuVyIsInB1YmxpY19qb2JzIjp0cnVlLCJjaV9jb25maWdfcGF0a"
                    "CI6bnVsbCwic2hhcmVkX3dpdGhfZ3JvdXBzIjpbXSwib25seV9hbGxvd19tZXJnZV9pZl9waXBl"
                    "bGluZV9zdWNjZWVkcyI6ZmFsc2UsInJlcXVlc3RfYWNjZXNzX2VuYWJsZWQiOmZhbHNlLCJvbmx"
                    "5X2FsbG93X21lcmdlX2lmX2FsbF9kaXNjdXNzaW9uc19hcmVfcmVzb2x2ZWQiOmZhbHNlLCJwcm"
                    "ludGluZ19tZXJnZV9yZXF1ZXN0X2xpbmtfZW5hYmxlZCI6dHJ1ZSwiYXBwcm92YWxzX2JlZm9yZ"
                    "V9tZXJnZSI6MH0="],
                   :chunked? false,
                   :headers #vcr-clj/clj-http-header-map
                   {"Cache-Control" "max-age=0, private, must-revalidate",
                    "Connection" "close",
                    "Content-Length" "1979",
                    "Content-Type" "application/json",
                    "Date" "Fri, 22 Sep 2017 16:16:19 GMT",
                    "Etag" "W/\"d0d15e3a64e64834cd77e0b31676585f\"",
                    "RateLimit-Limit" "600",
                    "RateLimit-Observed" "2",
                    "RateLimit-Remaining" "598",
                    "Server" "nginx",
                    "Strict-Transport-Security" "max-age=31536000",
                    "Vary" "Origin",
                    "X-Frame-Options" "SAMEORIGIN",
                    "X-Request-Id" "a91b97ca-ea34-49b9-b068-61fbaea3a5c6",
                    "X-Runtime" "2.343527"},
                   :length 1979,
                   :protocol-version {:major 1, :minor 1, :name "HTTP"},
                   :reason-phrase "Created",
                   :repeatable? false,
                   :status 201,
                   :streaming? true,
                   :trace-redirects []},
          :var-name "clj-http.core/request"}],
 :recorded-at #inst "2017-09-22T16:16:17.942-00:00"}
